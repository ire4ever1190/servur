package me.jakeleahy.servur

import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import me.jakeleahy.servur.Json.ServerInfo
import java.awt.Toolkit
import java.awt.datatransfer.Clipboard
import java.awt.datatransfer.StringSelection
import java.io.DataInputStream
import java.net.Socket
import java.nio.ByteBuffer


fun getServerInfo(host: String): ServerInfo {
    val encodedPayload = ByteBuilder()
            .addNull()
            .addInt(0)
            .addData(host)
            .addNull()
            .addInt(38)
            .addInt(1)
            .build()
    var client = Socket(host, 25565)
    // Send payload to server
    client.getOutputStream().write(encodedPayload.array())
    // Send ping byte to get response
    client.getOutputStream().write(ByteBuffer.allocate(2).put(1).put(0).array())

    // Gets the stream to receive info from server
    val inputStream = DataInputStream(client.getInputStream())
    // Reads the metadata
    val size = readVarInt(inputStream)
    val id = readVarInt(inputStream)
    val length = readVarInt(inputStream)

    // Converts to string
    // Removes trailing white space
    // TODO find better method
    val jsonString = String(inputStream.readAllBytes(), Charsets.UTF_8).split("}]}}")[0] + "}]}}"
    client.close()
    // parses JSON
    return jsonString.parseJson()
}

fun String.parseJson(): ServerInfo {
    val json = Json(JsonConfiguration.Stable)
    return json.parse(ServerInfo.serializer(), this)
}

@ExperimentalStdlibApi
fun main() {
   // println(getServerInfo("mc.jakeleahy.me"))
    println(Curse().search("random", 0))
}