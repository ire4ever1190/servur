package me.jakeleahy.servur

import kotlinx.serialization.Serializable

@Serializable
data class ServerInfo(
        val players: Players,
        val version: Version,
        val description: Description,
        val modinfo: ModList)

@Serializable
data class Players(val sample: List<Player>, val max: Int, val online: Int)

@Serializable
data class Version(val protocol: Int, val name: String)

@Serializable
data class Description(val text: String)

@Serializable
data class ModList(val modList: List<Mod>, val type: String)

@Serializable
data class Mod(val version: String, val modid: String)

@Serializable
data class Player(val id: String, val name: String)

