package me.jakeleahy.servur

import java.io.ByteArrayOutputStream
import java.io.DataInputStream
import java.io.DataOutputStream
import java.nio.ByteBuffer
import java.nio.ByteOrder
import kotlin.experimental.and
import kotlin.experimental.or

class ByteBuilder {
    var data: ArrayList<ByteBuffer> = ArrayList()

    fun addData(payload: String): ByteBuilder {
        val data = payload.toByteArray(Charsets.UTF_8)
        val encodedPayload = ByteBuffer.allocate(data.size + 1)
                .put(data.size.toByte())
                .put(data)
        this.data.add(encodedPayload)
        return this
    }

    fun addNull(): ByteBuilder {
        this.data.add(ByteBuffer.allocate(1).put(0))
        return this
    }

    fun addInt(number: Int): ByteBuilder {
        this.data.add(
                ByteBuffer.allocate(1)
                        .order(ByteOrder.BIG_ENDIAN)
                        .put(packVarint(number))
        )
        return this
    }

    fun build(): ByteBuffer {
        var totalSize = 0
        this.data.forEach { buf -> totalSize += buf.array().size }
        val buf = ByteBuffer.allocate(totalSize + 1)
        buf.put(packVarint(totalSize))
        this.data.forEach {
            it.array().forEach {
                buf.put(it)
            }
        }
        return buf

    }
}


fun packVarint(c: Int): ByteArray {
    var p = c
    val b = ByteArrayOutputStream()
    val s = DataOutputStream(b)
    while (true) {
        if ((p.toByte() and 0xFFFFFF80.toByte()) == 0.toByte()) {
            s.write(p)
            return b.toByteArray()
        }
        s.writeByte((p.toByte() and 0x7F.toByte() or 0x80.toByte()).toInt())
        p = p ushr 7
    }
}

fun readVarInt(input: DataInputStream): Int {
    var i = 0
    var j = 0
    while (true) {
        val k: Byte = input.readByte()
        i = i or ((k and 0x7F).toInt()) shl j++ * 7
        if (j > 5) throw RuntimeException("VarInt too big")
        if ((k and 0x80.toByte()) != 128.toByte()) break
    }
    return i
}