group = "me.jakeleahy"
version = "1.0"


repositories {
    jcenter()
}

plugins {
    application
    kotlin("jvm") version "1.3.70"
    kotlin("plugin.serialization") version "1.3.70"
}

dependencies {
    implementation(kotlin("stdlib"))
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime:0.20.0")
}

application {
    mainClassName = "me.jakeleahy.servur.MainKt"
}

sourceSets {
    main.get().java.srcDir("src/")
}